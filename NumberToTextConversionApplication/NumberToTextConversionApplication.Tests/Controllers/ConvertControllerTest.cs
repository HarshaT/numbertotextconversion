﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NumberToTextConversionApplication;
using NumberToTextConversionApplication.Controllers;

namespace NumberToTextConversionApplication.Tests.Controllers
{
	[TestClass]
	public class ConvertControllerTest
	{
		[TestMethod]
		public void Index()
		{
			//// Arrange
			ConvertController controller = new ConvertController();

			//// Act
			ViewResult result = controller.Index() as ViewResult;

			//// Assert
			Assert.IsNotNull(result);
		}		
	}
}
