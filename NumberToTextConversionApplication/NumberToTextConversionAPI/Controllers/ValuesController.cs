﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace NumberToTextConversionAPI.Controllers
{
	[EnableCors(origins: "*", headers: "*", methods: "*")]
	public class ValuesController : ApiController
	{
		// GET api/values
		public IEnumerable<string> Get()
		{
			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		public string Get(int id)
		{
			return "value";
		}

		// POST api/values
		public string Post(string name, double amount)
		{

			double number = amount;
			double cents = number - Math.Floor(number);
			cents = Math.Round(cents, 2) * 100;
			long dollarAmount = (long)Math.Floor(number);
			string dollarString = ConvertDollartoWords(dollarAmount);
			string centString = ConvertDollartoWords((long)cents);
			string pluralcentS = centString == "ONE" ? string.Empty : "S";
			string pluraldollarS = dollarString == "ONE" ? string.Empty : "S";
			string result = cents != 0 ?
				dollarString + " DOLLAR" + pluraldollarS + " AND " + centString + " CENT" + pluralcentS
									  :
				dollarString + " DOLLAR" + pluraldollarS;
			return result;
			//return null;
		}

		// PUT api/values/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}

		//A recursive function
		public string ConvertDollartoWords(long number)
		{
			if (number == 0) return "ZERO";

			string words = string.Empty;
			if ((number / 1000000) > 0)
			{
				//Calling the function from inside the function itself
				words += ConvertDollartoWords(number / 1000000) + " MILLION ";
				number %= 1000000;
			}
			if ((number / 1000) > 0)
			{
				words += ConvertDollartoWords(number / 1000) + " THOUSAND ";
				number %= 1000;
			}
			if ((number / 100) > 0)
			{
				words += ConvertDollartoWords(number / 100) + " HUNDRED ";
				number %= 100;
			}

			if (number > 0)
			{
				if (words != "") words += "AND ";
				// Need to initialize this as the numbers between 11-20 are not having conventional names i.e : Onety One , Onety two
				var zeroToNineteenArray = new[]
				{
					"ZERO",
					"ONE",
					"TWO",
					"THREE",
					"FOUR",
					"FIVE",
					"SIX",
					"SEVEN",
					"EIGHT",
					"NINE",
					"TEN",
					"ELEVEN",
					"TWELVE",
					"THIRTEEN",
					"FOURTEEN",
					"FIFTEEN",
					"SIXTEEN",
					"SEVENTEEN",
					"EIGHTEEN",
					"NINETEEN"
		};
				var tensArray = new[]
				{
					"ZERO",
					"TEN",
					"TWENTY",
					"THIRTY",
					"FORTY",
					"FIFTY",
					"SIXTY",
					"SEVENTY",
					"EIGHTY",
					"NINETY"
		};
				if (number < 20) words += zeroToNineteenArray[number];
				else
				{
					words += tensArray[number / 10];
					if ((number % 10) > 0) words += "-" + zeroToNineteenArray[number % 10];
				}
			}
			return words;
		}
	}
}
