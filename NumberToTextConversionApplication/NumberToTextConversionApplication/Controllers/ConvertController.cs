﻿using NumberToTextConversionApplication.Models;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace NumberToTextConversionApplication.Controllers
{
	public class ConvertController : Controller
	{
		public ActionResult Index()
		{
			return View("~/Views/ConvertNumber/ConvertNumber.cshtml");
		}		

		[HttpPost]
		public ActionResult ConvertNumber(MyDetails details)
		{
			string query = "?name=" + details.Name + "&amount=" + details.Amount;
			string url = "http://localhost:59293/api/Values" + query;
			WebClient client = new WebClient();

			string postData = "anything";
			client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
			// Upload the input string using the HTTP POST method.
			byte[] byteArray = System.Text.Encoding.ASCII.GetBytes(postData);
			byte[] byteResult = client.UploadData(url, "POST", byteArray);
			// Decode and display the result.
			var result = Encoding.ASCII.GetString(byteResult);
			ConversionResponse response = new ConversionResponse
			{
				Amount = result,
				Name = details.Name
			};
			return View("~/Views/ConvertNumber/ConvertNumber.cshtml", response);
		}

	}
}