﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NumberToTextConversionApplication.Models
{
    public class MyDetails
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }
    public class ConversionResponse
    {
        public string Name { get; set; }
        public string Amount { get; set; }
    }
}